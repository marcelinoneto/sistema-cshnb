class CreateReservesType < ActiveRecord::Migration
  def change
    create_table :reserves_type do |t|
      t.string :name
      t.boolean :deleted

      t.timestamps
    end
  end
end
