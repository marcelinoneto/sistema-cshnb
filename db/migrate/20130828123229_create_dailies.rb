class CreateDailies < ActiveRecord::Migration
  def change
    create_table :dailies do |t|
      t.float :daily
      t.boolean :deleted

      t.timestamps
    end
  end
end
