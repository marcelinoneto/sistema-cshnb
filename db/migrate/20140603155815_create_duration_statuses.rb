class CreateDurationStatuses < ActiveRecord::Migration
  def change
    create_table :duration_statuses do |t|
      t.string :role
      t.boolean :deleted

      t.timestamps
    end
  end
end
