class CreateLocalTypes < ActiveRecord::Migration
  def change
    create_table :local_types do |t|
      t.string :name
      t.float :price_local
      t.boolean :deleted

      t.timestamps
    end
  end
end
