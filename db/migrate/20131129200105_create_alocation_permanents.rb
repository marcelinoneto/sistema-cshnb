class CreateAlocationPermanents < ActiveRecord::Migration
  def change
    create_table :alocation_permanents do |t|
      t.integer :schedule
      t.integer :week_day
      t.boolean :deleted
      t.references :semester
      t.references :space
      t.references :sector

      t.timestamps
    end
    add_index :alocation_permanents, :space_id
    add_index :alocation_permanents, :semester_id
    add_index :alocation_permanents, :sector_id
  end
end