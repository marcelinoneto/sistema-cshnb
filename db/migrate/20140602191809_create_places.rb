class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.integer :capacity
      t.text :observations
      t.integer :size
      t.references :local_type
      t.references :block
      t.boolean :deleted

      t.timestamps
    end
    add_index :places, :local_type_id
    add_index :places, :block_id
  end
end
