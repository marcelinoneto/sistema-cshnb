class CreateDurations < ActiveRecord::Migration
  def change
    create_table :durations do |t|
      t.date :initiation
      t.date :end
      t.boolean :deleted
      t.string :act_number
      t.references :user
      t.references :function
      t.references :sector
      t.references :duration_status

      t.timestamps
    end
    add_index :durations, :user_id
    add_index :durations, :function_id
    add_index :durations, :sector_id
  end
end
