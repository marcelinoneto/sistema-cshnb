# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140603155815) do

  create_table "addresses", :force => true do |t|
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "district"
    t.string   "street"
    t.string   "numberzip_code"
    t.string   "complement"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "addresses", ["user_id"], :name => "index_addresses_on_user_id"

  create_table "administrators", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "alocation_permanents", :force => true do |t|
    t.integer  "schedule"
    t.integer  "week_day"
    t.boolean  "deleted"
    t.integer  "semester_id"
    t.integer  "space_id"
    t.integer  "sector_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "alocation_permanents", ["sector_id"], :name => "index_alocation_permanents_on_sector_id"
  add_index "alocation_permanents", ["semester_id"], :name => "index_alocation_permanents_on_semester_id"
  add_index "alocation_permanents", ["space_id"], :name => "index_alocation_permanents_on_space_id"

  create_table "blocks", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "confirmations", :force => true do |t|
    t.boolean  "approved"
    t.integer  "approver"
    t.text     "disapproval_justification"
    t.boolean  "deleted"
    t.integer  "request_id"
    t.integer  "user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "confirmations", ["request_id"], :name => "index_confirmations_on_request_id"
  add_index "confirmations", ["user_id"], :name => "index_confirmations_on_user_id"

  create_table "dailies", :force => true do |t|
    t.float    "daily"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "daily_function_locals", :force => true do |t|
    t.integer  "daily_id"
    t.integer  "function_id"
    t.integer  "local_id"
    t.boolean  "deleted"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "daily_function_locals", ["daily_id"], :name => "index_daily_function_locals_on_daily_id"
  add_index "daily_function_locals", ["function_id"], :name => "index_daily_function_locals_on_function_id"
  add_index "daily_function_locals", ["local_id"], :name => "index_daily_function_locals_on_local_id"

  create_table "drivers", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "duration_statuses", :force => true do |t|
    t.string   "role"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "durations", :force => true do |t|
    t.date     "initiation"
    t.date     "end"
    t.boolean  "deleted"
    t.string   "act_number"
    t.integer  "user_id"
    t.integer  "function_id"
    t.integer  "sector_id"
    t.integer  "duration_status_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "durations", ["function_id"], :name => "index_durations_on_function_id"
  add_index "durations", ["sector_id"], :name => "index_durations_on_sector_id"
  add_index "durations", ["user_id"], :name => "index_durations_on_user_id"

  create_table "functions", :force => true do |t|
    t.string   "name"
    t.integer  "level"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "holidays", :force => true do |t|
    t.string   "description"
    t.integer  "day"
    t.integer  "month"
    t.boolean  "fixed"
    t.date     "date"
    t.boolean  "deleted"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "institutions", :force => true do |t|
    t.string   "campus"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "local_types", :force => true do |t|
    t.string   "name"
    t.float    "price_local"
    t.boolean  "deleted"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "locals", :force => true do |t|
    t.string   "local"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "location_types", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "monthlies", :force => true do |t|
    t.date     "data"
    t.integer  "qtde"
    t.boolean  "transfer"
    t.integer  "driver_id"
    t.integer  "travel_id"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "monthlies", ["driver_id"], :name => "index_monthlies_on_driver_id"
  add_index "monthlies", ["travel_id"], :name => "index_monthlies_on_travel_id"

  create_table "participants", :force => true do |t|
    t.integer  "request_id"
    t.integer  "user_id"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "participants", ["request_id"], :name => "index_participants_on_request_id"
  add_index "participants", ["user_id"], :name => "index_participants_on_user_id"

  create_table "periods", :force => true do |t|
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "places", :force => true do |t|
    t.string   "name"
    t.integer  "capacity"
    t.text     "observations"
    t.integer  "size"
    t.integer  "local_type_id"
    t.integer  "block_id"
    t.boolean  "deleted"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "places", ["block_id"], :name => "index_places_on_block_id"
  add_index "places", ["local_type_id"], :name => "index_places_on_local_type_id"

  create_table "recesses", :force => true do |t|
    t.date     "begin"
    t.date     "end"
    t.string   "description"
    t.boolean  "deleted"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "requests", :force => true do |t|
    t.date     "date"
    t.date     "date_of_departure"
    t.date     "return_date"
    t.text     "goal"
    t.text     "justification"
    t.string   "destination"
    t.integer  "distance"
    t.boolean  "deleted"
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "requests", ["user_id"], :name => "index_requests_on_user_id"

  create_table "reservation_statuses", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reserves", :force => true do |t|
    t.string   "event"
    t.boolean  "free"
    t.boolean  "payment_confirmed"
    t.text     "justification"
    t.float    "price"
    t.integer  "approver"
    t.integer  "user_id"
    t.integer  "reservation_status_id"
    t.integer  "reserve_type_id"
    t.boolean  "deleted"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "reserves", ["reservation_status_id"], :name => "index_reserves_on_reservation_status_id"
  add_index "reserves", ["reserve_type_id"], :name => "index_reserves_on_reserve_type_id"
  add_index "reserves", ["user_id"], :name => "index_reserves_on_user_id"

  create_table "reserves_of_physical_space", :force => true do |t|
    t.date     "date"
    t.integer  "start_time"
    t.integer  "end_time"
    t.string   "event"
    t.text     "justification"
    t.boolean  "free"
    t.float    "price"
    t.boolean  "payment_confirmed"
    t.boolean  "deleted"
    t.integer  "user_id"
    t.integer  "space_id"
    t.integer  "reservation_status_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "approver"
  end

  add_index "reserves_of_physical_space", ["reservation_status_id"], :name => "index_reserves_of_physical_space_on_reservation_status_id"
  add_index "reserves_of_physical_space", ["space_id"], :name => "index_reserves_of_physical_space_on_space_id"
  add_index "reserves_of_physical_space", ["user_id"], :name => "index_reserves_of_physical_space_on_user_id"

  create_table "reserves_schedule_space", :force => true do |t|
    t.integer  "period_id"
    t.integer  "place_id"
    t.integer  "reserve_id"
    t.integer  "schedule"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "reserves_schedule_space", ["period_id"], :name => "index_reserves_schedule_space_on_period_id"
  add_index "reserves_schedule_space", ["place_id"], :name => "index_reserves_schedule_space_on_place_id"
  add_index "reserves_schedule_space", ["reserve_id"], :name => "index_reserves_schedule_space_on_reserve_id"

  create_table "reserves_type", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sectors", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "semesters", :force => true do |t|
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "spaces", :force => true do |t|
    t.string   "name"
    t.integer  "capacity"
    t.text     "observations"
    t.integer  "size"
    t.float    "price_local"
    t.boolean  "deleted"
    t.integer  "location_type_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "spaces", ["location_type_id"], :name => "index_spaces_on_location_type_id"

  create_table "travel_drivers", :force => true do |t|
    t.integer  "travel_id"
    t.integer  "driver_id"
    t.integer  "number_of_daily"
    t.boolean  "deleted"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "travel_drivers", ["driver_id"], :name => "index_travel_drivers_on_driver_id"
  add_index "travel_drivers", ["travel_id"], :name => "index_travel_drivers_on_travel_id"

  create_table "travel_expenses", :force => true do |t|
    t.float    "price"
    t.text     "justification"
    t.integer  "travel_id"
    t.boolean  "deleted"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "travel_expenses", ["travel_id"], :name => "index_travel_expenses_on_travel_id"

  create_table "travels", :force => true do |t|
    t.boolean  "status"
    t.integer  "request_id"
    t.integer  "vehicle_id"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "travels", ["request_id"], :name => "index_travels_on_request_id"
  add_index "travels", ["vehicle_id"], :name => "index_travels_on_vehicle_id"

  create_table "user_types", :force => true do |t|
    t.string   "type_name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "siape"
    t.string   "password"
    t.string   "email"
    t.string   "phone"
    t.boolean  "active"
    t.integer  "institution_id"
    t.integer  "sector_id"
    t.integer  "role_id"
    t.integer  "user_type_id"
    t.boolean  "deleted"
    t.string   "cpf"
    t.string   "registration"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "users", ["institution_id"], :name => "index_users_on_institution_id"
  add_index "users", ["role_id"], :name => "index_users_on_role_id"
  add_index "users", ["sector_id"], :name => "index_users_on_sector_id"

  create_table "vehicles", :force => true do |t|
    t.string   "carrier_plate"
    t.string   "vehicle_type"
    t.integer  "number_of_persons"
    t.boolean  "deleted"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

end
