#encoding: utf-8
class Duration < ActiveRecord::Base
  belongs_to :user
  belongs_to :function
  belongs_to :sector
  belongs_to :duration_status
  
  attr_accessible :deleted,
                  :acting,
                  :end,
                  :initiation,
                  :act_number,
                  :user_id,
                  :function_id,
                  :sector_id,
                  :duration_status_id

  validates :initiation, presence: true
  validates :function_id, presence: true
  validates :sector_id, presence: true
  validates :act_number, presence: true
  validates :is_public_employee, acceptance: {accept: true, message: ', Somente para Funcionário Público'}
  validates :no_two_bosses, acceptance: {accept: true, message: ', Já existe um chefe neste Departamento/Curso'}

  def is_public_employee
    if self.user.role_id == nil || self.user.siape == nil || self.user.user_type_id > 1
      return false 
    else
      return true
    end
  end

  def no_two_bosses
    boss = nil
    setor = Sector.find(self.user.sector_id)
    
    setor.users.each do |user|
      if !(user.durations.last.nil? && user.deleted == false)
        boss = user if (user.durations.last.function.level == 2 && user.durations.last.deleted == false)
      end
    end
    
    if (boss && self.function_id == 2)
      return false
    else
      return true      
    end
  end  
end