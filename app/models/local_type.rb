class LocalType < ActiveRecord::Base
  has_many :places, :dependent => :destroy
  attr_accessible :deleted, :name, :price_local, :places_attributes

  validates :name, presence: true
  validates :name, uniqueness:{case_sensitive: true} 
  validates :price_local, presence: true
end
