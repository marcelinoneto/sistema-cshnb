class TravelExpense < ActiveRecord::Base
  belongs_to :travel

  attr_accessible :justification, :price, :deleted, :travel_id

  validates :price, presence: true
  validates :justification, presence: true
  validates :travel_id, presence: true
end