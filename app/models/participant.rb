class Participant < ActiveRecord::Base
  belongs_to :request
  belongs_to :user
  
  attr_accessible :deleted,
                  :user_id, 
                  :request_id
end
