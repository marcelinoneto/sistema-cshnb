class Place < ActiveRecord::Base
  belongs_to :local_type
  belongs_to :block
  has_many :reserves_schedule_space, :dependent => :destroy
  attr_accessible :capacity, :deleted, :name, :observations, :size, :reserves_schedule_space_attributes, :local_type_id, :block_id

  validates :name, presence: true
  validates :capacity, presence: true
  validates :observations, presence: true
  validates :size, presence: true
  validates :local_type_id, presence: true
  validates :name, uniqueness:{case_sensitive: true}

end
