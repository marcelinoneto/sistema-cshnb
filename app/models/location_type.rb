class LocationType < ActiveRecord::Base
  has_many :spaces, :dependent => :destroy
  
  attr_accessible :deleted,
                  :name,
                  :spaces_attributes

  validates :name, presence: true
  validates :name, uniqueness:{case_sensitive: true} 
end
