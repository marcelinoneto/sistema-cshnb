class Sector < ActiveRecord::Base
  has_many :users, dependent: :destroy
  has_many :durations, dependent: :destroy
  has_many :alocation_permanents, dependent: :destroy

  attr_accessible :deleted,
                  :name,
                  :users_attributes,
                  :durations_attributes,
                  :alocation_permanents_attributes

  validates :name, uniqueness: true
  validates :name, presence: true
end
