class AlocationPermanent < ActiveRecord::Base
  belongs_to :space
  belongs_to :semester
  belongs_to :sector

  attr_accessible :deleted,
                  :schedule,
                  :week_day,
                  :space_id,
                  :semester_id,
                  :sector_id

  validates :schedule, presence: true
  validates :week_day, presence: true
end
