class ReserveType < ActiveRecord::Base
  has_many :reserves, :dependent => :destroy
  attr_accessible :deleted, :name, :reserves_attributes

  validates :name, presence: true
  validates :name, uniqueness:{case_sensitive: true}
  
end
