class Block < ActiveRecord::Base
  has_many :places, :dependent => :destroy
  attr_accessible :deleted, :name, :places_attributes

  validates :name, presence: true
  validates :name, uniqueness:{case_sensitive: true} 
end
