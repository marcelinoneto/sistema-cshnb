class ReserveScheduleSpace < ActiveRecord::Base
  belongs_to :period
  belongs_to :place
  belongs_to :reserve
  attr_accessible :deleted, :period_id, :place_id, :reserve_id
end
