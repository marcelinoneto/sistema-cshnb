class DailyFunctionLocal < ActiveRecord::Base
  belongs_to :daily
  belongs_to :function
  belongs_to :local
  
  attr_accessible :deleted,
                  :daily_id,
                  :function_id,
                  :local_id
end
