#encoding: utf-8
class Period < ActiveRecord::Base
  has_many :reserves_schedule_space, :dependent => :destroy
  attr_accessible :deleted, :end_date, :name, :start_date, :reserves_schedule_space_attributes

  validates :name, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :name, uniqueness:{case_sensitive: true}

  validates :data_do_fim, acceptance: {accept: true, message: ' menor que data do início ou campo está em branco'}
  validates :data_de_inicio_deste_periodo, acceptance: {accept: true, message: ' menor que data do fim do periodo anterior ou campo está em branco'}

  def data_do_fim
    return false if self.start_date.nil? || self.end_date.nil?
    if self.end_date < self.start_date
  	  return false
    else
   	  return true
    end
  end


  def data_de_inicio_deste_periodo
    period = Period.where(deleted: false).last
    return false if self.start_date.nil?

    if period.nil?
      return true
    else
      if self.start_date <= period.end_date
        return false
      else
        return true
      end  
    end
  end

end
