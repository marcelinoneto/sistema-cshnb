class Institution < ActiveRecord::Base
  has_many :users, dependent: :destroy
  has_many :amounts, dependent: :destroy
  
  attr_accessible :deleted, 
                  :campus,
                  :users_attributes,
                  :amounts_attributes
end
