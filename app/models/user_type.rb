class UserType < ActiveRecord::Base
  has_many :users, :dependent => :destroy
  attr_accessible :deleted, :type_name, :users_attributes
end
