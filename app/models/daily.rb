class Daily < ActiveRecord::Base
  has_many :daily_function_locals, dependent: :destroy
  
  attr_accessible :deleted,
                  :daily,
                  :daily_function_locals_attributes
end
