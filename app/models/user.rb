#encoding: utf-8
class User < ActiveRecord::Base

  belongs_to :institution
  belongs_to :sector
  belongs_to :role
  belongs_to :user_type
  has_many :durations, :dependent => :destroy
  has_many :functions, :through => :durations
  has_many :requests, :dependent => :destroy
  has_many :confirmations, :dependent => :destroy
  has_many :participants, :dependent => :destroy
  has_many :reserve_of_physical_spaces, :dependent => :destroy
  has_many :addresses, :dependent => :destroy
  has_many :reserves, :dependent => :destroy


  attr_accessible :active,
                  :email,
                  :name,
                  :password,
                  :phone,
                  :siape,
                  :institution_id,
                  :sector_id,
                  :role_id,
                  :user_type_id,
                  :durations_attributes,
                  :functions_attributes,
                  :reserve_of_physical_spaces_attributes,
                  :addresses_attributes,
                  :requests_attributes,
                  :confirmations_attributes, 
                  :participants_attributes,
                  :cpf,
                  :registration,
                  :deleted,
                  :password_confirmation,
                  :current_password,
                  :reserves_attributes


  attr_accessor :password_confirmation,
                :current_password
  
  validates :name, presence: true
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :cpf, presence: true
  validates :cpf, uniqueness: true
  validates :user_type_id, presence: true
  validates :institution_id, presence: true
  validates :siape, uniqueness: true
  validates :registration, uniqueness: true
  validates :is_public_employee, acceptance: {accept: true, message: ', Matrícula somente para usuários estudantes, SIAPE e Departamento obrigatórios para funcionários e docentes'}
  validates :is_student, acceptance: {accept: true, message: ', SIAPE somente para usuários funcionários e docentes, Matrícula e Departamento obrigatórios para estudantes'}
  validates :is_other, acceptance: {accept: true, message: ', Usuários externos não devem preencher Matricula nem SIAPE'}
 
  before_create do |user|
    user.active = true
    user.password = Digest::MD5.hexdigest(password)
  end

  before_save do |user|
    user.email.downcase!
  end


  def is_public_employee
    if self.user_type_id == 1 && (!self.registration.empty? || self.siape.empty? || self.sector_id.nil?)
      return false 
    else
      return true
    end
  end

  def is_student
    if self.user_type_id == 2 && (self.registration.empty? || !self.siape.empty?|| self.sector_id.nil?)
      return false 
    else
      return true
    end
  end

  def is_other
    if self.user_type_id == 3 && (!self.registration.empty? || !self.siape.empty?)
      return false 
    else
      return true
    end
  end
end
