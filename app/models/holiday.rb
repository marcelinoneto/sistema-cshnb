#encoding: utf-8
class Holiday < ActiveRecord::Base
  attr_accessible :deleted,
                  :date,
                  :day,
                  :description,
                  :fixed,
                  :month
  
  validates :description, presence: true
  validates :verificar_inteiros, acceptance: {accept: true, message: 'Data não é válida'}

  before_save do |holiday|
    holiday.date = nil if holiday.fixed
    holiday.day = nil && holiday.month = nil if holiday.fixed == false
  end
  
  def verificar_inteiros
  	ok = true
  	if self.fixed
      ok = false unless self.day
      ok = false if self.day && (self.day > 31 || self.day < 1)
      ok = false unless self.month
      ok = false if self.month && (self.month > 12 || self.month < 1)
  	end
    return ok
  end
end	