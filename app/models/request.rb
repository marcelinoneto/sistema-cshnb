#encoding: utf-8
class Request < ActiveRecord::Base
  belongs_to :user

  has_many :confirmations, :dependent => :destroy
  has_many :participants, :dependent => :destroy
  has_one :travel, :dependent => :destroy

  attr_accessible :deleted,
                  :date,
                  :date_of_departure,
                  :destination, :distance,
                  :goal, :justification, 
  				        :return_date,
                  :user_id,
                  :confirmations_attributes,
                  :participants_attributes,
  				        :travel_attributes

  validates :date, presence: true
  validates :date_of_departure , presence: true
  validates :return_date, presence: true
  validates :destination, presence: true
  validates :goal, presence: true
  validates :justification, presence: true
  validates :data_no_passado, acceptance: {accept: true, message: ' não é permitida'}
  validates :distancia_negativa, acceptance: {accept: true, message: ' ou campo em branco'}

  validates :user_id, presence: true #excluindo os terceirizados
  
  def dias
    (self.return_date - self.date_of_departure).to_i + 1
  end

  def meias_diarias
    # retorna o número de meias diárias
    return self.dias + (self.dias - 1)
  end

  def data_no_passado
    if self.date.nil?
      return false
    else
      if date_of_departure < DateTime.now.to_date or return_date < DateTime.now.to_date or return_date < date_of_departure 
        return false
      else
        return true
      end
    end
  end

  def distancia_negativa
    if self.distance.nil? || self.distance < 0
      return false
    else
      return true
    end
  end
end