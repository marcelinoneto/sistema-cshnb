#encoding: utf-8
class DurationsController < ApplicationController
  include AdministratorsHelper
  include UsersHelper
  include ApplicationHelper

  before_filter except: [] do
    if !((user_authenticated? && upper_lvl_2?) || administrator_authenticated?) 
      redirect_to notfound_path
    end
  end

  def index
    @user = User.find(params[:user_id])
    @durations = @user.durations.where(deleted: false)
  end

  def show
    @duration = Duration.find(params[:id])
  end

  def new
    @duration = Duration.new(user_id: params[:user_id])
  end

  def edit
    @duration = Duration.find(params[:id])
    @duration.initiation = format_date_br(@duration.initiation)
  end

  def create
    @duration = Duration.new(params[:duration])
    @duration.deleted = false
    laster = Duration.where(user_id: @duration.user_id).last

    if (administrator_authenticated? || (current_user.durations.last.function.level > @duration.function.level))

      laster.update_attributes(:end => Time.now) unless laster.nil?
      if @duration.save
        redirect_to user_durations_path(@duration.user_id)
      else
        render :new
      end  
    else
      @user = User.find(params[:duration][:user_id])
      @durations = @user.durations.where(deleted: false)
      flash[:warning] = 'Você só pode atribuir vigência de usuários de nível inferior ao seu!'
      render :new
    end
  end

  def update
    @duration = Duration.find(params[:id])
    f=Function.find(params[:duration][:function_id])
    if (administrator_authenticated? || ((current_user.durations.last.function.level > f.level) && (current_user.durations.last.function.level > @duration.function.level)))
      @duration.update_attributes(params[:duration])
      redirect_to user_durations_path(@duration.user_id)
    else
      flash[:warning] = 'Você só pode atribuir vigência de usuários de nível inferior ao seu!'
      render :edit
    end
  end

  def destroy
    @duration = Duration.find(params[:duration_id])
    if (administrator_authenticated? || (current_user.durations.last.function.level > @duration.function.level))
      @duration.update_attribute(:deleted, true)
      redirect_to user_durations_path(@duration.user_id)
    else
      @user = User.find(params[:id])
      @durations = @user.durations.where(deleted: false)
      flash[:warning] = 'Você só pode excluir vigência de usuários de nível inferior ao seu!'
      render :index
    end
  end
end
