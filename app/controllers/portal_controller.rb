class PortalController < ApplicationController
  include UsersHelper
  include AdministratorsHelper
  
  before_filter except: [:notfound] do
    if !administrator_authenticated? &&  !user_authenticated?
      redirect_to user_login_path
    elsif administrator_authenticated?
      render :home_administrator
    elsif user_authenticated?
      if diretor?
        render :home_diretor
      elsif caf?
        render :home_caf
      elsif chefe_setor_patrimonio?
        render :home_patrimonio
      elsif chefe_setor_transporte?
        render :home_transporte
      elsif usuario_nivel_1?
        render :home_nivel_1
      elsif usuario_nivel_2?
        render :home_nivel_2
      elsif aluno?
        render :home_aluno
      elsif externo?
        render :home_externo
      end
    end
  end

  def notfound
  end

  def index
  end

  def home_administrator
  end

  def home_diretor
  end

  def home_caf
  end

  def home_patrimonio
  end

  def home_transporte
  end

  def home_nivel_1
  end

  def home_nivel_2
  end

  def home_aluno
  end

  def home_externo
  end
end
