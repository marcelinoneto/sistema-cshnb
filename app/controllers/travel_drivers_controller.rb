class TravelDriversController < ApplicationController
  include MonthliesHelper
  include UsersHelper

  # before_filter except: [] do
  #   redirect_to home_path unless chefe_setor_transporte? || upper_lvl_3? 
  # end


  def list_monthlies
    @driver = Driver.find(params[:driver_id])
  end

  def index
    @travel_driver = TravelDriver.new(travel_id: params[:id])
    @travel_drivers = TravelDriver.where(deleted: false, travel_id: params[:id])
  end

  def show
    @travel_driver = TravelDriver.find(params[:id])
  end

  def new
    @travel_driver = TravelDriver.new
  end

  def edit
    @travel_driver = TravelDriver.find(params[:id])
  end

  def create
    @travel_driver = TravelDriver.new(params[:travel_driver])
    @travel_driver.deleted = false

    x = TravelDriver.where(travel_id: @travel_driver.travel_id, driver_id: @travel_driver.driver_id)
    
    if x.empty?
      if @travel_driver.save
        driver = Driver.find(@travel_driver.driver_id)
        @travel_driver.travel.request.meias_diarias.to_i.times{
          Monthly.create(travel_id: @travel_driver.travel_id, driver_id: @travel_driver.driver_id, transfer: false, deleted: false, data: @travel_driver.travel.request.date_of_departure)
        }
        driver.save 
        redirect_to add_driver_path(@travel_driver.travel_id)
      else
        redirect_to add_driver_path(@travel_driver.travel_id)
      end
    else
      x.first.update_attributes(deleted: false)
      mensalidades = Monthly.where(travel_id: x.first.travel_id, driver_id: x.first.driver_id)
      
      mensalidades.each do |monthly|
        monthly.update_attributes(deleted: false)
      end
      redirect_to add_driver_path(@travel_driver.travel_id)      
    end
  end

  def update
    @travel_driver = TravelDriver.find(params[:id])

    if @travel_driver.update_attributes(params[:travel_driver])
      redirect_to travel_drivers_path
    else
      render :edit
    end
  end

  def destroy
    @travel_driver = TravelDriver.find(params[:id])
    @travel_driver.update_attributes(deleted: true)
    
    mensalidades = Monthly.where(travel_id: @travel_driver.travel_id, driver_id: @travel_driver.driver_id)
    
    mensalidades.each do |monthly|
      monthly.update_attributes(deleted: true)
    end
    redirect_to add_driver_path(@travel_driver.travel_id)
  end

  def add_drivers
    motorista_viagem = TravelDriver.new(driver_id: params[:driver][:id], travel_id: params[:id], deleted: false)
    if motorista_viagem.save
      redirect_to add_driver_path(params[:id]) 
    else
      redirect_to add_driver_path(params[:id])
    end
  end
end

