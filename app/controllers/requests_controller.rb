class RequestsController < ApplicationController

  include ApplicationHelper
  include UsersHelper

  before_filter only: [] do
    if user_authenticated?
      redirect_to notfound_path unless externo?
    else
      redirect_to notfound_path
    end
  end

  def index
    if user_authenticated?
      @requests = Request.where(deleted: false, user_id: session[:user].id)
    else
      redirect_to notfound_path
    end
  end

  def show
    @request = Request.find(params[:id])
  end

  def new
    if user_authenticated?
       @request = Request.new
    else
       redirect_to notfound_path
    end
  end

  def edit
    @request = Request.find(params[:id])
    redirect_to notfound_path if !user_authenticated? || session[:user].id != @request.user_id || !@request.confirmations.empty?
    @request.date_of_departure = format_date_br(@request.date_of_departure)
    @request.return_date = format_date_br(@request.return_date)
  end

  def create
    @request = Request.new(params[:request])
    @request.deleted = false
    @request.user_id = session[:user].id  
    @request.date = DateTime.now
    if @request.save
      redirect_to requests_path
    else
      render :new
    end
  end

  def update
    @request = Request.find(params[:id])
     
    if !user_authenticated? || session[:user].id != @request.user_id || !@request.confirmations.empty?
      redirect_to notfound_path  
    elsif @request.update_attributes(params[:request])
      redirect_to requests_path
    else
      render :edit
    end
  end

  def destroy
    @request = Request.find(params[:id])
    @request.update_attribute(:deleted, true)
    redirect_to requests_path
  end
end
