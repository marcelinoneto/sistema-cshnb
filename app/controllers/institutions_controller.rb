class InstitutionsController < ApplicationController
 
  include AdministratorsHelper
  
  before_filter except: [] do
    unless administrator_authenticated?
      redirect_to notfound_path
    end
  end

  def index
    @institutions = Institution.where(deleted: false)
  end

  def show
    @institution = Institution.find(params[:id])
  end

  def new
    @institution = Institution.new
  end

  def edit
    @institution = Institution.find(params[:id])
  end

  def create
    @institution = Institution.new(params[:institution])
    @institution.deleted = false
    if @institution.save
      redirect_to institutions_path
    else
      render :new
    end
  end

  def update
    @institution = Institution.find(params[:id])

    if @institution.update_attributes(params[:institution])
      redirect_to institutions_path
    else
      render :edit
    end
  end

  def destroy
    @institution = Institution.find(params[:id])
    @institution.update_attribute(:deleted, true)
    redirect_to institutions_path
  end
end
