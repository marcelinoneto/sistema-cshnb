class MonthliesController < ApplicationController
  include UsersHelper
  
  before_filter except: [] do
    if user_authenticated?
      redirect_to home_path unless chefe_setor_transporte? || upper_lvl_3?
    else
      redirect_to notfound_path
    end
  end
  
  
  def index
    @driver_id = params[:id]
    @monthlies = Monthly.where(driver_id: params[:id], transfer: false)
    @monthlies2 = Monthly.where(driver_id: params[:id], transfer: true)
  end

  def show
    @monthly = Monthly.find(params[:id])
  end

  def new
    @monthly = Monthly.new
  end

  def edit
    @monthly = Monthly.find(params[:id])
  end

  def create
    @monthly = Monthly.new(params[:monthly])

    if @monthly.save
      redirect_to monthlies_path
    else
      render :new
    end
  end

  def update
    @monthly = Monthly.find(params[:id])

    if @monthly.update_attributes(params[:monthly])
      redirect_to monthlies_path
    else
      render :edit
    end
  end

  def destroy
    @monthly = Monthly.find(params[:id])
    @monthly.update_attribute(:deleted, true)
    redirect_to monthlies_path
  end

  def repasse
    a = params[:diarias].to_a
    
    a.each do |x|
      y = Monthly.find(x.last)
      y.update_attributes(transfer: true)
    end

    redirect_to monthlies_repasse_path(params[:driver_id])
  end

  def cancelar_repasse
    @monthly = Monthly.find(params[:id])
    
    @monthly.update_attributes(transfer: false)

    redirect_to monthlies_repasse_path(params[:driver_id])
  end
end
