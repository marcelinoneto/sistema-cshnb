#encoding: utf-8
class UsersController < ApplicationController

  include UsersHelper
  include AdministratorsHelper

  # before_filter :usuario_logado, only: [:login]

  # before_filter only: [:new, :create] do
  #   redirect_to notfound_path unless upper_lvl_3?
  # end

  before_filter only: [:login] do
   if user_authenticated?
      redirect_to root_path
    end 
  end

  before_filter only: [:edit_profile, :update_profile, :edit_password, :update_password] do
    unless user_authenticated?
      redirect_to notfound_path    
    end
  end

  before_filter only: [:new, :create, :index, :show, :edit, :update] do
    unless (user_authenticated? && upper_lvl_2?) || administrator_authenticated?
      redirect_to notfound_path
    end
  end

=begin
  before_filter only: [:authentication] do
    redirect_to notfound_path unless 
  end
=end

  def index
    @users = User.where(deleted: false)
  end

  def login
    @user = User.new
  end

  def logout
    end_session_user
    redirect_to root_path
  end

  def authentication
    @user = User.new(params[:user])
    if authenticate_user(@user)
      redirect_to root_path
    else
      render :login
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def new 
    if administrator_authenticated? || (user_authenticated? && current_user.durations.last.function.level>=2)
      @pwd = code_generate
      @user = User.new
    else 
      redirect_to notfound_path
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(params[:user])
      if administrator_authenticated? || (user_authenticated? && (current_user.durations.last.function.level>=3 || (current_user.durations.last.function.level==2 && current_user.sector_id == @user.sector_id)))
      @user.deleted = false
      if @user.save
        redirect_to users_path
      else
        @pwd = code_generate
        render :new
      end
    else 
      @pwd = code_generate
      flash[:warning] = 'Você só pode cadastrar usuários do seu setor e de nível inferior ao seu! '
      render :new
    end

    
  end

  def update
    @user = User.find(params[:id])
    if (administrator_authenticated? || (user_authenticated? && (current_user.durations.last.function.level>=3 || (current_user.durations.last.function.level==2 && current_user.sector_id == @user.sector_id ))))
      if @user.update_attributes(params[:user])
        redirect_to users_path
      else
        render :edit
      end
    else
      flash[:warning] = 'Você só pode alterar usuários do seu setor e de nível inferior ao seu!'
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    if (administrator_authenticated? || (user_authenticated? && (current_user.durations.last.function.level>=3 || (current_user.durations.last.function.level==2 && current_user.sector_id == @user.sector_id ))))
      @user.update_attributes(deleted: true)
      redirect_to users_path
    else
      @users = User.where(deleted: false)
      flash[:warning] = 'Você só pode excluir usuários do seu setor e de nível inferior ao seu!'
      render :index
    end
  end

  def edit_profile
    @user = User.find(session[:user].id)
  end

  def update_profile
    @user = User.find(session[:user].id)
    if @user.update_attributes(params[:user])
      redirect_to root_path
    else
      render :edit_profile
    end
  end

  def edit_password
    @user = User.new
  end

  def update_password
    @user = User.new(params[:user])
    if change_password(@user.current_password, @user.password, @user.password_confirmation)
      redirect_to user_logout_path
    else
      render :edit_password
    end
  end
end
