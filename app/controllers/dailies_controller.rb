class DailiesController < ApplicationController

  def index
    @dailies = Daily.where(deleted: false)
  end

  def show
    @daily = Daily.find(params[:id])
  end

  def new
    @daily = Daily.new
  end

  def edit
    @daily = Daily.find(params[:id])
  end

  def create
    @daily = Daily.new(params[:daily])
    @daily.deleted = false

    if @daily.save
      redirect_to dailies_path
    else
      render :new
    end
  end

  def update
    @daily = Daily.find(params[:id])

    if @daily.update_attributes(params[:daily])
      redirect_to dailies_path
    else
      render :edit
    end
  end

  def destroy
    @daily = Daily.find(params[:id])
    @daily.update_attribute(:deleted, true)
    redirect_to dailies_path
  end
end
