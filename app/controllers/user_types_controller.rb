class UserTypesController < ApplicationController

  include AdministratorsHelper

  before_filter except: [] do
    redirect_to notfound_path unless administrator_authenticated?
  end
  
  def index
    @user_types = UserType.where(deleted: false)
  end

  def show
    @user_type = UserType.find(params[:id])
  end

  def new
    @user_type = UserType.new
  end

  def edit
    @user_type = UserType.find(params[:id])
  end

  def create
    @user_type = UserType.new(params[:user_type])
    @user_type.deleted = false
    
    if @user_type.save
      redirect_to user_types_path
    else
      render :new
    end
  end

  def update
    @user_type = UserType.find(params[:id])

    if @user_type.update_attributes(params[:user_type])
      redirect_to user_types_path
    else
      render :edit
    end    
  end

  def destroy
    @user_type = UserType.find(params[:id])
    @user_type.update_attribute(:deleted, true)
    redirect_to user_types_path
  end
end