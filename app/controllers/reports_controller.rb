class ReportsController < ApplicationController
  include ReserveOfPhysicalSpacesHelper
  def imprimir_veiculos
    @vehicles = Vehicle.all

    respond_to do |format|
      format.html
      format.pdf do
        html = render_to_string layout: false, action: "imprimir_veiculos.pdf.erb", formats: [:html,:pdf], handler: [:erb]
        kit = PDFKit.new(html)
        kit.stylesheets << "#{Rails.root}/public/bootstrap.css"
        send_data kit.to_pdf, filename: "veiculos.pdf", type: "application/pdf", disposition: "inline"
        return #to avoid double call
      end
    end
  end
  def imprimir_salas
    @space = Space.all
    respond_to do |format|
      format.html
      format.pdf do
        html = render_to_string layout: false, action: "imprimir_salas.pdf.erb", formats: [:html,:pdf], handler: [:erb]
        kit = PDFKit.new(html, :orientation => 'Portrait')
        #kit = PDFKit.new(html, :orientation => 'Landscape')
        kit.stylesheets << "#{Rails.root}/public/bootstrap.css"
        send_data kit.to_pdf, filename: "salas.pdf", type: "application/pdf", disposition: "inline"
        return 
      end
    end
  end  
  def imprimir_s
    @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: params[:semester_id])
    @id = params[:id]
    
    @array = Array.new(16).map {|e| e = Array.new(7).map {|ee| e = '---' } }

    @reserves.each do |r|
      @array[r.schedule][r.week_day] = 'OCUPADO'
    end
    
    @espaco = Space.find(@id)
    @semester = Semester.find(params[:semester_id])
    respond_to do |format|
      format.html
      format.pdf do
        html = render_to_string layout: false, action: "imprimir_s.pdf.erb", formats: [:html,:pdf], handler: [:erb]
        #kit = PDFKit.new(html, :orientation => 'Portrait')
        kit = PDFKit.new(html, :orientation => 'Landscape', :page_size => 'A4')
        kit.stylesheets << "#{Rails.root}/public/bootstrap.css"
        send_data kit.to_pdf, filename: "sala#{@espaco.name}.pdf", type: "application/pdf", disposition: "inline"
        return 
      end
    end
  end 
  def imprimir_s_m
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    @space = Space.find(params[:id])
    @id = params[:id]
    semester = Semester.where(deleted: false)
    @reserves = []

    semester.each do |s|
      if (s.start_date.year == ano && s.end_date.year == ano) && (mes >= s.start_date.month && mes <= s.end_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)        
      elsif (s.start_date.year < ano) && (s.end_date.year == ano) && (mes <= s.end_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)
      elsif (s.start_date.year == ano) && (mes >= s.start_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)
      end
    end

    @holiday_fixeds = Holiday.where(deleted: false, fixed: true, month: mes)
    @holiday_move = Holiday.where(deleted: false, fixed: false)
    
    @holiday_move = @holiday_move.select { |h| h.date.year == ano }

    @recesso = Recess.where(deleted: false)
    @reserve_of_physical_spaces = ReserveOfPhysicalSpace.where(deleted: false, space_id: params[:id], reservation_status_id: 1)

    @recesso = @recesso.select {|h| h.begin.month == mes || h.end.month == mes }

    @holiday_move = @holiday_move.select {|h| h.date.month == mes} 

    @reserve_of_physical_spaces = @reserve_of_physical_spaces.select {|r| r.date.month == mes && r.date.year == ano}

    x = DateTime.new(ano, mes, 1)
    @qtd_dias_mes = x.end_of_month.day
    @ano = ano
    @mes = mes
    @array = Array.new(16).map {|e| e = Array.new(@qtd_dias_mes).map {|ee| e = '' } }
    
    @reserves.each do |r|
        a = dias_reserva_fixa(mes, ano, r.week_day, @qtd_dias_mes, r.semester.start_date, r.semester.end_date)
        a.each do |w|
          @array[r.schedule][w-1] = r
        end
    end
    @recesso.each do |r|
      if r.begin.day >= 1 && r.end.day <= @qtd_dias_mes && (r.begin.year == ano || r.end.year == ano)
        for i in (r.begin.day .. r.end.day)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end
      if (r.begin.year == ano && r.end.year == ano) && (r.begin.month == mes && r.end.month != mes)
         for i in (r.begin.day .. @qtd_dias_mes)
           for j in (0..15)
             @array[j][i-1] = r
           end
         end
      end
      if (r.begin.year == ano && r.end.year == ano) && (r.begin.month != mes && r.end.month == mes)
         for i in (1 .. r.end.day)
           for j in (0..15)
             @array[j][i-1] = r
           end
         end
      end
      if (((r.end.day >= 1 && r.begin.month < mes && r.begin.year == ano) || (r.end.day >= 1 && r.begin.month > mes && r.begin.year < ano)) && (r.begin.year == ano - 1))
        for i in (1 .. r.end.day)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end
      if (((r.begin.day >= 1 && r.end.month > mes && r.end.year == ano) || (r.begin.day >= 1 && r.end.month < mes && r.end.year > ano)) && (r.end.year == ano + 1))
        for i in (r.begin.day .. @qtd_dias_mes)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end
    end
    @holiday_fixeds.each do |h|
      for i in (0..15)
        @array[i][h.day - 1] = h
      end
    end
    @holiday_move.each do |h|
      for i in (0..15)
        @array[i][h.date.day - 1] = h
      end
    end
    @reserve_of_physical_spaces.each do |r|
      for i in (r.start_time .. r.end_time)
        @array[i][r.date.day - 1] = r
      end
    end
    respond_to do |format|
      format.html
      format.pdf do
        html = render_to_string layout: false, action: "imprimir_s_m.pdf.erb", formats: [:html,:pdf], handler: [:erb]
        #kit = PDFKit.new(html, :orientation => 'Portrait')
        kit = PDFKit.new(html, :orientation => 'Landscape', :page_size => 'A4')
        kit.stylesheets << "#{Rails.root}/public/bootstrap.css"
        send_data kit.to_pdf, filename: "sala#{@space.name}/#{months[@mes-1]}.pdf", type: "application/pdf", disposition: "inline"
        return 
      end
    end
  end   
end
