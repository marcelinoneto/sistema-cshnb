class LocalTypesController < ApplicationController

   def index
     @local_types = LocalType.where(deleted: false)
   end

   def new
     @local_type = LocalType.new
   end

   def edit
     @local_type = LocalType.find(params[:id])
   end

   def create
     @local_type = LocalType.new(params[:local_type])
     @local_type.deleted = false
   
     if @local_type.save
       redirect_to local_types_path  
     else
       render :new
     end
   end

  def update
    @local_type = LocalType.find(params[:id])
  
    if @local_type.update_attributes(params[:local_type])
      redirect_to local_types_path
    else
      render :edit
    end
  end

  def destroy
    @local_type = LocalType.find(params[:id])    
    @local_type.update_attribute(:deleted, true)
    redirect_to local_types_path
  end

end
