class BlocksController < ApplicationController

   def index
    @blocks = Block.where(deleted: false)
   end

   def new
    @block = Block.new
   end

   def edit
    @block = Block.find(params[:id])
   end

   def create
    @block = Block.new(params[:block])
    @block.deleted = false
   
    if @block.save
      redirect_to blocks_path  
    else
      render :new
    end
   end

   def update
    @block = Block.find(params[:id])
   
    if @block.update_attributes(params[:block])
      redirect_to blocks_path
    else
      render :edit
    end
   end

  def destroy
    @block = Block.find(params[:id])    
    @block.update_attribute(:deleted, true)
    redirect_to blocks_path
  end

end
