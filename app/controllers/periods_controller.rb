class PeriodsController < ApplicationController

  include ApplicationHelper
   
   def index
    @periods = Period.where(deleted: false)
   end

   def new
    @period = Period.new
   end

   def edit
    @period = Period.find(params[:id])
    @period.start_date = format_date_br(@period.start_date)
    @period.end_date = format_date_br(@period.end_date)
   end

   def create
    @period = Period.new(params[:period])
    @period.deleted = false
   
    if @period.save
      redirect_to periods_path  
    else
      render :new
    end
   end


   def update
    @period = Period.find(params[:id])

    if @period.update_attributes(params[:period])
      redirect_to periods_path
    else
      render :edit
    end
  end


  def destroy
    @period = Period.find(params[:id])
    @period.update_attribute(:deleted, true)
    redirect_to periods_path
  end

end
