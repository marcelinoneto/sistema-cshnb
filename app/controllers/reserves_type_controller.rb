class ReservesTypeController < ApplicationController

   def index
     @reserves_type = ReserveType.where(deleted: false)
   end

   def new
     @reserve_type = ReserveType.new
   end

   def edit
     @reserve_type = ReserveType.find(params[:id])
   end

   def create
     @reserve_type = ReserveType.new(params[:reserve_type])
     @reserve_type.deleted = false
   
     if @reserve_type.save
       redirect_to reserves_type_path  
     else
       render :new
     end
   end

   def update
     @reserve_type = ReserveType.find(params[:id])
   
     if @reserve_type.update_attributes(params[:reserve_type])
       redirect_to reserves_type_path
     else
       render :edit
     end
   end

  def destroy
    @reserve_type = ReserveType.find(params[:id])    
    @reserve_type.update_attribute(:deleted, true)
    redirect_to reserves_type_path
  end

end
