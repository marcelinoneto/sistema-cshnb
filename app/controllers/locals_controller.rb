class LocalsController < ApplicationController
  include UsersHelper
  include AdministratorsHelper

  before_filter except: [] do
    unless (user_authenticated? && upper_lvl_3?) || administrator_authenticated?
      redirect_to notfound_path
    end
  end
  
  def index
    @locals = Local.where(deleted: false)
  end

  def show
    @local = Local.find(params[:id])
  end

  def new
    @local = Local.new
  end

  def edit
    @local = Local.find(params[:id])
  end

  def create
    @local = Local.new(params[:local])
    @local.deleted = false
    
    if @local.save
      redirect_to locals_path
    else
      render :new
    end
  end

  def update
    @local = Local.find(params[:id])

    if @local.update_attributes(params[:local])
      redirect_to locals_path
    else
      render :edit
    end
  end

  def destroy
    @local = Local.find(params[:id])
    @local.update_attribute(:deleted, true)
    redirect_to locals_path
  end
end
