class DailyFunctionLocalsController < ApplicationController
  include UserHelper
  include AdministratorHelper


  # before_filter only: [:index, :show] do
  #   redirect_to notfound_path unless user_authenticated?
  # end

  # before_filter except: [:index, :show] do
  #   redirect_to notfound_path unless user_authenticated?
  # end

  before_filter except: [] do
    if !((user_authenticated? && upper_lvl_3?) || administrator_authenticated?)
      redirect_to notfound_path
    end
  end
  
  def index
    @daily_function_locals = DailyFunctionLocal.where(deleted: false)
  end

  def show
    @daily_function_local = DailyFunctionLocal.find(params[:id])
  end

  def new
    @daily_function_local = DailyFunctionLocal.new
  end

  def edit
    @daily_function_local = DailyFunctionLocal.find(params[:id])
  end

  def create
    @daily_function_local = DailyFunctionLocal.new(params[:daily_function_local])
    @daily_function_local.deleted = false

    if @daily_function_local.save
      redirect_to daily_function_locals_path
    else
      render :new
    end
  end

  def update
    @daily_function_local = DailyFunctionLocal.find(params[:id])

    if @daily_function_local.update_attributes(params[:daily_function_local])
      redirect_to daily_function_locals_path
    else
      render :edit
    end
  end

  def destroy
    @daily_function_local = DailyFunctionLocal.find(params[:id])
    @daily_function_local.update_attribute(:deleted, true)
    redirect_to daily_function_locals_path
  end
end
