class AlocationPermanentsController < ApplicationController
  
  include AlocationPermanentsHelper
  include UsersHelper

  # before_filter :autorizacao_chefe_setor_patrimonio, except: []

  before_filter except: [] do
    if user_authenticated?
      redirect_to notfound_path unless chefe_setor_patrimonio? || upper_lvl_3?
    else
      redirect_to notfound_path
    end
  end

  def index
    @semesters = Semester.where(deleted: false)
  end

  def espacos
    @semester_id = params[:semester_id]
    @spaces = Space.where(deleted: false)
    @semester = Semester.find(@semester_id)
  end

  def reserves
    @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: params[:semester_id])
    @id = params[:id]
    #@semester_id = params[:semester_id]
    #@reserves = AlocationPermanent.where(deleted: false)
    @array = Array.new(16).map {|e| e = Array.new(7).map {|ee| e = '---' } }

    @reserves.each do |r|
      @array[r.schedule][r.week_day] = 'OCUPADO'
    end
    
    @espaco = Space.find(@id)
    @semester = Semester.find(params[:semester_id])
  end


  def editar
    @space = Space.find(params[:id])
    @semester = Semester.find(params[:semester_id])
    @array = Array.new(16).map {|e| e = Array.new(7).map {|ee| e = false } }

    reserves = AlocationPermanent.where(deleted: false, space_id: @space.id, semester_id: @semester.id)

    for r in reserves
      @array[r.schedule][r.week_day] = true
    end
  end



  def salvar_alocacao
    a = params[:alocations].to_a

    #a = a.reject { |h| h[0] == "semester_id" }
    a = a.map { |h| h[1] if h[1].has_key? :ok }
    a = a.reject { |h| h.nil? }
        
    array = Array.new

    a.each do |h|
      alocacoes = AlocationPermanent.where(schedule: h[:schedule], week_day: h[:weekday], space_id: params[:space_id], semester_id: params[:semester_id]).first
      # puts "ACONTECEU ALGUMA COISA AQUI #{K}"
      if alocacoes.nil?
        array << AlocationPermanent.create(deleted: false, schedule: h[:schedule], week_day: h[:weekday], space_id: params[:space_id], semester_id: params[:semester_id]).id
      else 
        alocacoes.update_attributes(deleted: false) if alocacoes.deleted == true
        array << alocacoes.id
      end
    end

    deletar = AlocationPermanent.where(semester_id: params[:semester_id], space_id: params[:space_id]).where('"alocation_permanents"."id" NOT IN (?)', array)

    deletar.each do |d|
      d.update_attributes(deleted: true)
    end

    redirect_to alocation_permanents_space_path(params[:space_id],params[:semester_id])
  end
  
  def show
    @fixed_reserf = AlocationPermanent.find(params[:id])
  end

  def new
    @fixed_reserf = AlocationPermanent.new
  end

  def edit
    @fixed_reserf = AlocationPermanent.find(params[:id])
  end

  def create
    @alocation_permanent = AlocationPermanent.new(params[:fixed_reserve])

    if @alocation_permanent.save
      redirect_to alocation_permanents_path
    else
      render :new
    end
  end

  def update
    @alocation_permanent = AlocationPermanent.find(params[:id])

    if @alocation_permanent.update_attributes(params[:@alocation_permanent])
      redirect_to alocation_permanents_path
    else
      render :edit
    end
  end

  def destroy
    @alocation_permanent = AlocationPermanent.find(params[:id])
    @alocation_permanent.update_attribute(:deleted, true)
    redirect_to alocation_permanents_path
  end
end
