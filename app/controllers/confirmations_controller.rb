#encoding: utf-8
class ConfirmationsController < ApplicationController

  include UsersHelper

  before_filter except: [] do
    if user_authenticated?
      redirect_to home_path unless chefe_setor_transporte? || upper_lvl_3?
    else
      redirect_to notfound_path
    end
  end

  def index
    @requests = Request.where(deleted: false).order("id DESC")
  end

  def show
    @confirmation = Confhomehomeirmation.find(params[:id]) 
  end

  def new
    @confirmation = Confirmation.new
  end

  def edit  
    @confirmation = Confirmation.find(params[:id])
  end

  # def create
  #   @confirmation = Confirmation.new(params[:confirmation])
  #   @confirmation.deleted = false
    
  #   if @confirmation.save
  #     redirect_to confirmations_path
  #   else
  #     render :new
  #   end
  # end

  # def update
  #   @confirmation = Confirmation.find(params[:id])

  #   if @confirmation.update_attributes(params[:confirmation])
  #     redirect_to confirmations_path
  #   else
  #     render :edit
  #   end
  # end

  def destroy
    @confirmation = Confirmation.find(params[:id])
    @confirmation.update_attribute(:deleted, true)
    redirect_to confirmations_path
  end

  def approve
    @requests = Request.where(deleted: false).order("id DESC")
    @request = Request.find(params[:id])
    
    if !(@request.confirmations.count == 0)
      confirmation = @request.confirmations.first
      usuario = User.find(confirmation.approver)
      if session[:user].durations.last.function.level >= usuario.durations.last.function.level
        confirmation.update_attributes(approved: true, approver: session[:user].id)
      else
        flash[:warning] = 'Essa reserva foi modificada por um usuário de nivel maior que o seu. Por isso não pode ser modificada'
      end
    else 
      Confirmation.create(approved: true, request_id: @request.id, user_id: session[:user].id, approver: session[:user].id)
    end
    #redirect_to confirmations_path
    render :index
  end

  def non_approve
    @requests = Request.where(deleted: false).order("id DESC")
    @request = Request.find(params[:id])
    
    if !(@request.confirmations.count == 0)
      confirmation = @request.confirmations.first
      usuario = User.find(confirmation.approver)
      if session[:user].durations.last.function.level >= usuario.durations.last.function.level
         confirmation.update_attributes(approved: false, disapproval_justification: params[:confirmation][:disapproval_justification], approver: session[:user].id)
      else
         flash[:warning] = 'Essa reserva foi modificada por um usuário de nivel maior que o seu. Por isso não pode ser modificada'
      end
    else 
      Confirmation.create(approved: false, request_id: @request.id, user_id: session[:user].id, disapproval_justification: params[:confirmation][:disapproval_justification], approver: session[:user].id)
    end 
    #redirect_to confirmations_path 
    render :index
  end

  def non_approve_form
    @request = Request.find(params[:id])
    
    if (@request.confirmations.count  == 0)
      @confirmation = Confirmation.new
    else 
      @confirmation = @request.confirmations.first
    end 
  end

  def travel
    r =  Request.find(params[:id])

    if r.travel.nil?
      redirect_to confirmation_new_travel_path(r.id)
    else
      redirect_to r.travel
    end
  end
end