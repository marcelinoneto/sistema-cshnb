class SemestersController < ApplicationController

  include UsersHelper
  include ApplicationHelper

  # before_filter :autorizacao_chefe_setor_patrimonio, except: []
  before_filter except: [] do
    if user_authenticated?
      redirect_to notfound_path unless chefe_setor_patrimonio? || upper_lvl_3?
    else
      redirect_to notfound_path
    end
  end

  def index
    @semesters = Semester.where(deleted: false)
  end

  def show
    @semester = Semester.find(params[:id])
  end

  def new
    @semester = Semester.new
  end

  def edit
    @semester = Semester.find(params[:id])
    @semester.start_date = format_date_br(@semester.start_date)
    @semester.end_date = format_date_br(@semester.end_date)
  end

  def create
    @semester = Semester.new(params[:semester])
    @semester.deleted = false

    if @semester.save
      redirect_to semesters_path  
    else
      render :new
    end
  end

  def update
    @semester = Semester.find(params[:id])

    if @semester.update_attributes(params[:semester])
      redirect_to semesters_path
    else
      render :edit
    end
  end
  
  def destroy
    @semester = Semester.find(params[:id])    
    @semester.update_attribute(:deleted, true)
    redirect_to semesters_path
  end
end
